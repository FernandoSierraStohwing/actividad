
package cl.prueba;


public class Medicamento extends Producto {
    //07 crear atributos de la clase Medicamento
    protected boolean isGenerico;
    
    //08 crear constructores

    public Medicamento() {
    }
    //8.1 la palabra super hace referencia que esta instanciando los atributos de la super clase
    public Medicamento(boolean isGenerico, String codigo, String nombre, int precioBase) {
        super(codigo, nombre, precioBase);
        this.isGenerico = isGenerico;
    }

    //09 crear metodos getter y setter (solo los de esta clase)

    public boolean isIsGenerico() {
        return isGenerico;
    }

    public void setIsGenerico(boolean isGenerico) {
        this.isGenerico = isGenerico;
    }
    //10 crear una clase Formulado
    
    
    
    
    
    
    
    
}
