
package cl.prueba;


public class SuplementoAlimenticio extends Producto{
    //15 crear atributos de la clase
    protected int cantidadVitaminas;
    protected String informacionVitaminas;
    
    //16 crear constructores

    public SuplementoAlimenticio() {
    }

    public SuplementoAlimenticio(int cantidadVitaminas, String informacionVitaminas, String codigo, String nombre, int precioBase) {
        super(codigo, nombre, precioBase);
        this.cantidadVitaminas = cantidadVitaminas;
        this.informacionVitaminas = informacionVitaminas;
    }
     //17 crear getter y setter

    public int getCantidadVitaminas() {
        return cantidadVitaminas;
    }

    public void setCantidadVitaminas(int cantidadVitaminas) {
        this.cantidadVitaminas = cantidadVitaminas;
    }

    public String getInformacionVitaminas() {
        return informacionVitaminas;
    }

    public void setInformacionVitaminas(String informacionVitaminas) {
        this.informacionVitaminas = informacionVitaminas;
    }
    //18 crear una clase RegistroProducto
    
    
}
