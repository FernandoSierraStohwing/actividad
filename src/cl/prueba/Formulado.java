
package cl.prueba;


public class Formulado extends Medicamento{
    //11 crear atributos de la clase
    protected String contraindicaciones;
    
    //12 crear constructores

    public Formulado() {
    }

    public Formulado(String contraindicaciones, boolean isGenerico, String codigo, String nombre, int precioBase) {
        super(isGenerico, codigo, nombre, precioBase);
        this.contraindicaciones = contraindicaciones;
    }

    //13 crear getter y setter

    public String getContraindicaciones() {
        return contraindicaciones;
    }

    public void setContraindicaciones(String contraindicaciones) {
        this.contraindicaciones = contraindicaciones;
    }
    
    //14 crear clase SuplementoAlimenticio
    
    
    
    
    
    
}
