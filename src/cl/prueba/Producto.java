package cl.prueba;

public abstract class Producto {
  //01 crear atributos de la clase (Producto)   
    protected String codigo, nombre;
    protected int precioBase;
    
  //02 crear constructores con y sin parametros
    //2.1 me permite instanciar objetos vacios
    public Producto() {
    }
    //2.2 me permite instanciar objetos con sus respectivos atributos
    public Producto(String codigo, String nombre, int precioBase) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.precioBase = precioBase;
    }
    
    //03 crear metodos getter(obtener) y setter(modificar)

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getPrecioBase() {
        return precioBase;
    }

    public void setPrecioBase(int precioBase) {
        this.precioBase = precioBase;
    }
    //04 crear una interface llamada controlable
    

}
